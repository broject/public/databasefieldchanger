Vue.component('fieldchanger-string', require('./components/String.vue').default);
Vue.component('fieldchanger-text', require('./components/Text.vue').default);
Vue.component('fieldchanger-boolean', require('./components/Boolean.vue').default);
Vue.component('fieldchanger-select', require('./components/Select.vue').default);
Vue.component('fieldchanger-date', require('./components/Date.vue').default);
Vue.component('fieldchanger-datetime', require('./components/DateTime.vue').default);
Vue.component('fieldchanger-autocomplete', require('./components/Autocomplete.vue').default);

Vue.component('reactive-value', require('./components/ReactiveValueDisplay.vue').default);

window.DatabaseFieldchangerEventBus = new Vue();