<?php

use Faker\Generator as Faker;
use pbro\DatabaseFieldchanger\Tests\MockedModel;
use pbro\DatabaseFieldchanger\Tests\MockedModelWithoutRules;

$factory->define(MockedModelWithoutRules::class, function (Faker $faker) {
    return [
        'string' => $faker->words(rand(1, 6), true),
    ];
});