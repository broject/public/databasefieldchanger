<?php

use Faker\Generator as Faker;
use pbro\DatabaseFieldchanger\Tests\MockedModel;

$factory->define(MockedModel::class, function (Faker $faker) {
    return [
        'string' => $faker->words(rand(1, 6), true),
    ];
});