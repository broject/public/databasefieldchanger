<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePbroMockedModelsTable extends Migration
{
    public function up()
    {
        Schema::create('pbro_mocked_models', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('string');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pbro_mocked_models');
    }
}