# DatabaseFieldchanger

## Description
A package that gives you the ability to change single model attributes without pressing the save button.

## Extends

---

## Installation
This package can be installed via composer:

```bash
composer require pbro/database-fieldchanger
```

## Requirements

---

## Usage

---

### Publishables

1. Config
```bash
php artisan vendor:publish --provider="pbro\DatabaseFieldchanger\DatabaseFieldchangerServiceProvider" --tag="config"
```

2. Vue components
```bash
php artisan vendor:publish --provider="pbro\DatabaseFieldchanger\DatabaseFieldchangerServiceProvider" --tag="vue"
```

### Configuration

---

### Customizability

---

## Usable Resources
> Listing and definition of resources that can be used outside the package.

### Actions

---

### Permissions

---

### Events

---

### Scopes

---

### Traits

---

### Factories

---

### Vue Components

---

### API Routes

Route | Description
| --- | --- | 
/component/v1/pbro/database-fieldchanger/change | Executes the attribute change

### Helpers

---

### Commands

---

### Exceptions

---

## Uninstallation

1. composer remove pbro/database-fieldchanger
