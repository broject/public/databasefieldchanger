<?php

use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')
    ->prefix('/component/v1/pbro/databasefieldchanger/')
    ->group(__DIR__.'/component.php');