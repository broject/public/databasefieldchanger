<?php

use Illuminate\Support\Facades\Route;
use pbro\DatabaseFieldchanger\Http\Controllers\Component\ChangeController;

Route::post('/change', ChangeController::class)->name('pbro-databasefieldchanger-change');