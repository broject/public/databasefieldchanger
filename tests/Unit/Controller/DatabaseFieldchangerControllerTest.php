<?php

namespace pbro\DatabaseFieldchanger\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use pbro\DatabaseFieldchanger\Tests\MockedModel;
use pbro\DatabaseFieldchanger\Tests\MockedModelWithoutRules;
use pbro\DatabaseFieldchanger\Tests\TestCase;

class DatabaseFieldchangerControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_change_a_value()
    {
        $mockedModel = factory(MockedModel::class)->create(['string' => 'old value']);

        $response = $this->json('POST', route('pbro-databasefieldchanger-change'), [
            'classname' => get_class($mockedModel),
            'entity_id' => $mockedModel->id,
            'fieldname' => 'string',
            'new_value' => 'new value',
            'old_value' => $mockedModel->string,
        ]);

        $response
            ->assertStatus(200)
            ->assertJson(['string' => 'new value']);

        $this->assertEquals('new value', $mockedModel->fresh()->string);
    }

    /** @test */
    public function cant_change_the_value_if_class_does_not_exist()
    {
        $mockedModel = factory(MockedModel::class)->create(['string' => 'old value']);

        $response = $this->json('POST', route('pbro-databasefieldchanger-change'), [
            'classname' => 'This\Class\Does\Not\Exist',
            'entity_id' => $mockedModel->id,
            'fieldname' => 'string',
            'new_value' => 'new value',
            'old_value' => $mockedModel->string,
        ]);

        $response
            ->assertStatus(\Symfony\Component\HttpFoundation\Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson(['message' => 'The given data was invalid.']);
    }

    /** @test */
    public function cant_change_the_value_if_entity_does_not_exist()
    {
        $mockedModel = factory(MockedModel::class)->create(['string' => 'old value']);

        $response = $this->json('POST', route('pbro-databasefieldchanger-change'), [
            'classname' => get_class($mockedModel),
            'entity_id' => $mockedModel->id + 1,
            'fieldname' => 'string',
            'new_value' => 'new value',
            'old_value' => $mockedModel->string,
        ]);

        $response
            ->assertStatus(\Symfony\Component\HttpFoundation\Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson(['message' => 'The given data was invalid.']);
    }

    /** @test */
    public function cant_change_the_value_if_fieldname_does_not_exist()
    {
        $mockedModel = factory(MockedModel::class)->create(['string' => 'old value']);

        $response = $this->json('POST', route('pbro-databasefieldchanger-change'), [
            'classname' => get_class($mockedModel),
            'entity_id' => $mockedModel->id,
            'fieldname' => 'idontexist',
            'new_value' => 'new value',
            'old_value' => $mockedModel->string,
        ]);

        $response
            ->assertStatus(\Symfony\Component\HttpFoundation\Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson(['message' => 'The given data was invalid.']);
    }

    /** @test */
    public function cant_change_the_value_if_model_does_not_have_rules()
    {
        $mockedModelWithoutRules = factory(MockedModelWithoutRules::class)->create(['string' => 'old value']);

        $response = $this->json('POST', route('pbro-databasefieldchanger-change'), [
            'classname' => get_class($mockedModelWithoutRules),
            'entity_id' => $mockedModelWithoutRules->id,
            'fieldname' => 'string',
            'new_value' => 'new value',
            'old_value' => $mockedModelWithoutRules->string,
        ]);

        $response
            ->assertStatus(\Symfony\Component\HttpFoundation\Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson(['message' => 'The given data was invalid.']);
    }

    /** @test */
    public function cant_change_the_value_if_value_validation_fails()
    {
        $mockedModel = factory(MockedModel::class)->create(['string' => 'old value']);

        $response = $this->json('POST', route('pbro-databasefieldchanger-change'), [
            'classname' => get_class($mockedModel),
            'entity_id' => $mockedModel->id,
            'fieldname' => 'string',
            'new_value' => '',
            'old_value' => $mockedModel->string,
        ]);

        $response
            ->assertStatus(\Symfony\Component\HttpFoundation\Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson(['message' => 'The given data was invalid.']);
    }

    /** @test */
    public function cant_change_the_value_if_value_has_been_changed_in_the_meantime()
    {
        $mockedModel = factory(MockedModel::class)->create(['string' => 'old value']);

        $response = $this->json('POST', route('pbro-databasefieldchanger-change'), [
            'classname' => get_class($mockedModel),
            'entity_id' => $mockedModel->id,
            'fieldname' => 'string',
            'new_value' => 'new value',
            'old_value' => 'different value than the value in db',
        ]);

        $response
            ->assertStatus(\Symfony\Component\HttpFoundation\Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson(['message' => 'The value has been changed in the meantime.']);
    }
}