require('jsdom-global')();

global.expect = require('expect');
global.axios = require('axios');
global.moxios = require('moxios');
global.Vue = require('vue');
global.bus = new Vue();

window.Date = Date;
