import {shallow, shallowMount} from 'vue-test-utils'
import DatabaseFieldchanger from "../../resources/js/components/DatabaseFieldchanger.vue";

describe('DatabaseFieldchanger', () => {

    it('should have some properties', function () {
        let wrapper = shallow(DatabaseFieldchanger);

        expect(wrapper.vm.$options.props.classname.required).toBeTruthy();
        expect(wrapper.vm.$options.props.entity_id.required).toBeTruthy();
        expect(wrapper.vm.$options.props.fieldname.required).toBeTruthy();
        expect(wrapper.props().new_value).toBeNull();
        expect(wrapper.props().old_value).toBeNull();
    });

    it('should take the given inputs', function () {
        const data = {
            classname: 'pbro\\DatabaseFieldchanger\\Tests\\MockedModel',
            entity_id: 1,
            fieldname: 'string',
            new_value: 'new value',
            old_value: 'old value',
        };

        let wrapper = shallow(DatabaseFieldchanger, {
            propsData: data,
        });

        expect(wrapper.props().classname).toBe(data.classname);
        expect(wrapper.props().entity_id).toBe(data.entity_id);
        expect(wrapper.props().fieldname).toBe(data.fieldname);
        expect(wrapper.props().new_value).toBe(data.new_value);
        expect(wrapper.props().old_value).toBe(data.old_value);
    });

    it('should have some default data', function () {
        const data = {
            classname: 'pbro\\DatabaseFieldchanger\\Tests\\MockedModel',
            entity_id: 1,
            fieldname: 'string',
            new_value: 'new value',
            old_value: 'old value',
        };

        let wrapper = shallow(DatabaseFieldchanger, {
            propsData: data,
        });

        expect(wrapper.vm.$data.isLoading).toBeFalsy();
        expect(wrapper.vm.$data.error).toBeNull();
    });

    it('should change the value after successful axios call', function () {
        moxios.install();

        const data = {
            classname: 'pbro\\DatabaseFieldchanger\\Tests\\MockedModel',
            entity_id: 1,
            fieldname: 'string',
            new_value: 'new value',
            old_value: 'old value',
        };

        let wrapper = shallow(DatabaseFieldchanger, {
            propsData: data,
        });

        moxios.stubRequest('/component/v1/pbro/databasefieldchanger/change', {
            status: 200,
            response: {
                string: 'new value'
            }
        });

        wrapper.vm.change();

        moxios.uninstall();
    })

});