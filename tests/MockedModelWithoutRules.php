<?php

namespace pbro\DatabaseFieldchanger\Tests;

use Illuminate\Database\Eloquent\Model;

class MockedModelWithoutRules extends Model
{
    protected $table = 'pbro_mocked_models';
}