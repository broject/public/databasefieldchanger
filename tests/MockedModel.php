<?php

namespace pbro\DatabaseFieldchanger\Tests;

use Illuminate\Database\Eloquent\Model;

class MockedModel extends Model
{
    protected $table = 'pbro_mocked_models';

    public $rules = [
        'string' => 'required|string|min:2|max:255',
    ];
}