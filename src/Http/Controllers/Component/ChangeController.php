<?php

namespace pbro\DatabaseFieldchanger\Http\Controllers\Component;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;

class ChangeController
{
    public function __invoke(Request $request)
    {
        // validate parameters
        $this->validateGeneral($request);

        // generate model
        $model = $this->generateModel($request->classname);

        // find entity
        $entity = $this->findEntity($model, $request->entity_id);

        // check if model does have the given fieldname in database table
        if (!in_array($request->fieldname, Schema::getColumnListing($model->getTable()))) {
            Log::error('Fieldchanger: Fieldname does not exist for this model', $request->all());
            return Response::json(['message' => 'An error occured'], \Symfony\Component\HttpFoundation\Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        // check if model does have defined rules for field
        if (empty($model->rules) || !in_array($request->fieldname, array_keys($model->rules))) {
            Log::error('Fieldchanger: Rule for the given fieldname is not defined', $request->all());
            return Response::json(['message' => 'An error occured'], \Symfony\Component\HttpFoundation\Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        // validate the value
        $validator = Validator::make([$request->fieldname => $request->new_value], [
            $request->fieldname => $model->rules[$request->fieldname],
        ]);

        // Check if value is acceptable
        if ($validator->fails()) {
            return Response::json([
                'message' => $validator->errors()->first(),
            ], \Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST);
        }

        // check if value has been changed in the meantime
        //dd($entity[$request->fieldname], $request->old_value);
        if ($entity[$request->fieldname] != $request->old_value) {
            return Response::json([
                'message' => 'The value has been changed in the meantime.',
                $request->fieldname => $entity->fresh()[$request->fieldname],
            ], \Symfony\Component\HttpFoundation\Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        // change and save value
        if ($entity[$request->fieldname] !== $request->new_value) {
            $entity[$request->fieldname] = $request->new_value;
            $entity->save();
        }

        // return new value for given fieldname
        return Response::json([
            $request->fieldname => $entity[$request->fieldname]
        ], \Symfony\Component\HttpFoundation\Response::HTTP_OK);
    }

    public function validateGeneral(Request $request)
    {
        $request->validate([
            'classname' => 'required|string|class',
            'entity_id' => 'required|integer|entity_exists:classname',
            'fieldname' => 'required|string',
            'current_value' => 'nullable',
        ]);
    }

    public function generateModel(string $classname): Model
    {
        return App::make($classname);
    }

    public function findEntity(Model $model, int $entity_id)
    {
        return  $model->find($entity_id);
    }
}
