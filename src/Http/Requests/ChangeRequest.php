<?php

namespace pbro\DatabaseFieldchanger\Http\Requests;

use Illuminate\Http\Request;

class ChangeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'classname' => 'required|string|class',
            'entity_id' => 'required|integer|entity_exists:classname',
            'fieldname' => 'required|string',
            'current_value' => 'nullable',
        ];
    }
}