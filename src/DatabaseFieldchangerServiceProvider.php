<?php

namespace pbro\DatabaseFieldchanger;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class DatabaseFieldchangerServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->migration();
        $this->routes();
    }

    public function boot()
    {
        $this->customValidation();
    }

    public function migration()
    {
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations/2019_09_09_000000_create_pbro_mocked_models_table.php');
    }

    public function routes()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/routes.php');
    }

    public function customValidation()
    {
        Validator::extend('class', function ($attribute, $value, $parameters, $validator) {
            return !!class_exists($value);
        }, 'Class does not exist');

        Validator::extend('entity_exists', function ($attribute, $value, $parameters, $validator) {
            $data = $validator->getData();

            if (!$class = $data[$parameters[0]]) {
                return false;
            }

            if (!class_exists($class)) {
                return false;
            }

            $model = App::make($class);
            $entity = $model::find($value);

            return ($model && $entity);
        }, 'Entity does not exist');
    }
}